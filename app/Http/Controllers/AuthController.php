<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view("page.form");
    }

    public function welcome(Request $request)
    {
        $firstName = $request['firstName'];
        $lastName = $request['lastName'];
        $token = $request['_token'];
        return view('page/.welcome', compact('firstName', 'lastName'));
    }
}
