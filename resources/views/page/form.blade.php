<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>

        <h2>Sign Up Form</h2>

        <form action="welcome" method="POST">

            @csrf

            <label for="firstName">First Name:</label><br><br>
            <input type="text" name="firstName" id="firstName"><br><br>
            
            <label for="lastName"> Last Name:</label><br><br>
            <input type="text" name="lastName" id="lastName"><br><br>
            
            <label for="gender"> Gender</label><br><br>
            <input type="radio" name="gender" id="male"><label for="male">Male</label><br>
            <input type="radio" name="gender" id="female"><label for="female">Female</label><br>
            <input type="radio" name="gender" id="other"><label for="other">Other</label><br><br>
            
            <label for="nationality">Nationality</label><br><br>
            <select name="nationality" id="nationality">br
                <option value="indonesia">Indonesia</option>
                <option value="malaysia">Malaysia</option>
                <option value="singapore">Singapore</option>
            </select><br><br>
            
            <label for="languageSpoken">Language Spoken:</label><br><br>
            <input type="checkbox" name="languageSpoken" id="bahasaIndonesia"><label for="bahasaIndonesia">Bahasa Indonesia</label><br>
            <input type="checkbox" name="languageSpoken" id="english"><label for="english">English</label><br>
            <input type="checkbox" name="languageSpoken" id="other"><label for="other">Other</label><br><br>

            <label for="bio">Bio</label><br><br>
            <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
            
            <input type="submit" value="Sign Up">

        </form>
</body>
</html>
